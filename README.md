# Best time to buy and sell stock project

## Description

Designed algorithm is for finding maximum profit of the transaction (i.e., buy one and sell one share of the stock).

## Algorithm

The minimum element is assigned the value of the first element of the prices array and maximum profit is 0. Iterating through the array, it is checked if the array i-th element is less than the minimum element. If it's true then the minimum element is assigned the value of the array i-th element. If it's not then it's checked whether the difference between the array i-th element and the minimum element is greater than the maximum profit, and if so, then the value of this difference is assigned to the maximum profit.

#### Time complexity: O(n)
#### Space complexity: O(1)

## Submission results

![](submissions.PNG)