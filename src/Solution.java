public class Solution {
    public static void main(String[] args) {
        int[] pricesArray = {7,1,5,3,6,4};
        System.out.println(maxProfit(pricesArray));
    }
    private static int maxProfit(int[] prices) {
        if(prices.length == 0 || prices.length == 1){
            return 0;
        }
        int minEl = prices[0];
        int maxProfit = 0;
        for(int i = 1; i < prices.length; i++){
            if(prices[i] < minEl){
                minEl = prices[i];
            }
            else if(maxProfit<prices[i]-minEl){
                maxProfit = prices[i]-minEl;
            }
        }
        return maxProfit;
    }
}

